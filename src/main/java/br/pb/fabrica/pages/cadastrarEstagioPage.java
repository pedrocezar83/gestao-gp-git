package br.pb.fabrica.pages;

import org.openqa.selenium.By;

import br.pb.fabrica.core.BasePage;
/*
 *  Analista de Testes: Pedro Carvalho 
 *  Data: 13/10/2020
 *  
 */
public class cadastrarEstagioPage extends BasePage{

	/********** Formulário **********/ 

	public void clicarBotaoCadastro() {
		
		clicarBotao(By.xpath("//*[@id='accordionSidebar']/li[4]/a/span"));
	}
	
	public void AcessarTelaCadastro() {
		clicarLink("Cadastro");
	}
	
	
	public void setConcedente(String concedente) {
		selecionarCombo(By.id("id_tipo_de_convenio"), concedente);
	}

	public void setCurso(String curso) {
		selecionarCombo(By.id("id_curso"), curso);
	}

	public void setDisciplina(String disciplina) {
		selecionarCombo(By.id("id_disciplina"), disciplina);
	}				

	public void setTurno(String turno) {
		selecionarCombo(By.id("id_turno"), turno);
	}
	
	public void setQuantidadeDeAlunos(String texto) {
		escrever(By.id("id_quantidade_de_alunos"), texto);
	}

	public void setCustoPorAluno(String texto) {
		escrever(By.id("id_custo_por_aluno"), texto);
	}

	public void setPreceptor(String preceptor) {
		selecionarCombo(By.id("id_preceptor"), preceptor);
	}
	
	public void setLocal(String local) {
		selecionarCombo(By.id("id_local"), local);
	}
	
	public void setTipoDeEstabelecimento(String tipo) {
		selecionarCombo(By.id("id_tipo_de_estabelecimento"), tipo);
	}
	
	public void setSetor(String setor) {
		escrever(By.id("id_setor"), setor);
	}

	public void setData(String data) {
		escrever(By.id("id_dates"), data);
	}
	
	public void clicarBotaoEnviar() {
		clicarBotao(By.xpath("//button[text()='Enviar']"));
	}
	
	
	
	public String obterTextoCurso() {
		return obterTexto(By.xpath("//*[@id=\"content\"]/div[2]/table/tbody/tr/td[1]/a"));
	}
	
	public String obterTextoDisciplina() {
		return obterTexto(By.xpath("//*[@id=\"content\"]/div[2]/table/tbody/tr/td[2]"));
	}
	
	public String obterTextoTipoConvenio() {
		return obterTexto(By.xpath("//*[@id=\"content\"]/div[2]/table/tbody/tr/td[3]"));
	}
	
	public String obterTextoPreceptor() {
		return obterTexto(By.xpath("//*[@id=\"content\"]/div[2]/table/tbody/tr/td[4]"));
	}
	
	public String obterTextoTurno() {
		return obterTexto(By.xpath("//*[@id=\"content\"]/div[2]/table/tbody/tr/td[5]"));
	}	
	
	public String obterTextoSetor() {
		return obterTexto(By.xpath("//*[@id=\"content\"]/div[2]/table/tbody/tr/td[6]"));
	}
	
	public String obterTextoQuantidadeAlunos() {
		return obterTexto(By.xpath("//*[@id=\"content\"]/div[2]/table/tbody/tr/td[7]"));
	}
	
	public String obterDiasDoEstagio() {
		return obterTexto(By.xpath("//*[@id=\"content\"]/div[2]/table/tbody/tr/td[8]"));
	}
	
	public String obterDataDeInicio() {
		return obterTexto(By.xpath("//*[@id=\"content\"]/div[2]/table/tbody/tr/td[9]"));
	}
	
	public String obterDataDeTermino() {
		return obterTexto(By.xpath("//*[@id=\"content\"]/div[2]/table/tbody/tr/td[10]"));
	}	
}