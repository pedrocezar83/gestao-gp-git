package br.pb.fabrica.pages;

import br.pb.fabrica.core.BasePage;

public class CriarCursoPage extends BasePage {
	
	
	public void setCurso (String nomeCurso) {
		
		escreverPorXpath("//*[@id='id_curso']", nomeCurso);
	}
	
	public void clickBtnEnviar() {
		
		clicarBotao("//button[@type='submit']");
	}

}
